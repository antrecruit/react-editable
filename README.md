# React editable

Goals of the assessment
-----------------------
* Gauge problem-solving skills
* Gauge developer skills (git / js / React)
* Gauge UI / UX skills
* Work distribution / delegation
* Ability to look "beyond the question"
* Time management
* Code quality

Assignment
----------
For our Rheocube product, we need a reuseable component to edit text contents "inline", rather than opening a form or dialog screen. The client is available to further clarify requirements and approve on the design. You will also have a React co-developer available. Results shall be pushed to a separate Git branch.

Reference implementations:
* [Trello](https://trello.com)
* [React{ions} inline edit](https://reactions.getambassador.com/components/inline-edit)

Time and phases
---------------
* Phase 0: Intro and setup
* Phase 1 (15-30mins): Assignment, requirements, design
* Phase 2 (90mins): Co-development, problem solving
* Phase 3 (15-30mins): Evaluate and further improvements

Available resources
-------------------
* Clock
* Wifi
* Whiteboard
* Laptop with Code editor, NPM and Git
* Basic Git repo to get started
* Client and co-developer

Getting started
---------------
```npm start```

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits. You will also see any lint errors in the console.
