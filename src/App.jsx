import React from 'react'

// import Editable from './Editable'

class App extends React.Component {
  render() {
    return (
      <div style={{width: '80%', maxWidth: 600, margin: '2rem auto'}}>
        <h1>Editable title</h1>
        <p>
          Editable lorem ipsum dolor sit amet, consectetur adipiscing elit, 
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
      </div>
    )
  }
}

export default App